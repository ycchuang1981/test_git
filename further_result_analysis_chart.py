# coding:utf-8

'''
結果比較 已經報告完後的資料視覺化練習與找尋發送後的學習
增加一些說明


'''
from __future__ import unicode_literals
import pandas as pd
import numpy as np
import os.path
#pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
# plt.xkcd()
import matplotlib

myfont = matplotlib.font_manager.FontProperties(fname='/System/Library/Fonts/PingFang.ttc')  
myfont.set_size('large')

###test
#matplotlib.rcParams['axes.unicode_minus']=False  
#plt.plot([-1,2,-5,3])  
#plt.title(u'中文',fontproperties=myfont)  
#plt.show()
###test

matplotlib.style.use('ggplot')
os.chdir('/Users/Jack/workspace/toyota_poc/')

#df_click = pd.read_csv(u'./cutted/TOYOTA 電子特函 Backstage_1205雲智匯.csv',  sep=",", encoding="utf-8")




def df_get_data():
    df_click = pd.read_csv(u'./cutted/Sienta電子特邀函_雲智匯0106.csv',  sep=",", encoding="utf-8")
    
    df_click['click'] = 1
    df_click[df_click[u'兌換狀態']=='Y'].shape[0]
    
    #RESULT_ALL3_toErica 10316_cr_for_joint_no_space 改上增加特徵的
    df_10316 = pd.read_csv(u'./cutted/10316_cr_for_joint_no_space.csv',  sep=",", encoding="utf-8")
    df_10316['is_in10316'] = 1
    df_10316[u'年式'] = df_10316[u'年式'].astype(str)
    df_10316[u'最近回廠年'] = df_10316[u'最近回廠年'].astype(str)

    df_10000 = pd.read_csv(u'./cutted/1萬筆_經銷商_to雲智匯(正確經銷商)_fix.csv',  sep=",", encoding="utf-8")
 

    #更新里程數   最近回廠年
    # df_10000[u'最近回廠里程數'] = df_10000[u'最近回廠里程數'].fillna(0).astype(float)
    # df_10000[u'最近回廠年'] = df_10000[u'最近回廠年'].fillna(0).astype(float)
    # df_10000[u'年式'] = df_10000[u'年式'].fillna(0).astype(float)

    # df_10000[u'year_diff'] = df_10000[u'最近回廠年'] - df_10000[u'年式']

    # df_10000[u'最近回廠里程數'] = df_10000.apply (lambda x: ((2016.0-x[u'最近回廠年'])*(x[u'最近回廠里程數']/x[u'year_diff'])+x[u'最近回廠里程數']) if x[u'year_diff']>0 else x[u'最近回廠里程數'], axis=1).astype(int)




    df_10000[u'車名'] = df_10000[u'車名'].map(lambda x: x.replace(' ', ''))
    df_10000[u'年式'] = df_10000[u'年式'].astype(str)
    df_10000[u'最近回廠年'] = df_10000[u'最近回廠年'].astype(str)
    
    df_10000[u'使用人年齡'] = df_10000[u'使用人年齡'].map(lambda x: int(x/10)*10  ).astype(str)
    df_10000[u'最近回廠里程數'] = df_10000[u'最近回廠里程數'].map(lambda x: int(x/50000)*50000  ).astype(str)



    df_30000 = pd.read_csv(u'./cutted/3萬筆_經銷商to雲智匯_fix.csv',  sep=",", encoding="utf-8")
    df_30000[u'使用人手機'] = df_30000[u'使用人手機'].str[-6:]
    df_30000[u'投保車體險或任意險'] = df_30000[u'投保車體險或任意險'].map(lambda x: u'Y' if str(x)=='V' else u'N')
    df_30000[u'車名'] = df_30000[u'車名'].map(lambda x: x.replace(' ', ''))
    df_30000[u'年式'] = df_30000[u'年式'].astype(str)
    df_30000[u'最近回廠年'] = df_30000[u'最近回廠年'].astype(str)
    
    df_30000[u'使用人年齡'] = df_30000[u'使用人年齡'].map(lambda x: int(x/10)*10  ).astype(str)
    df_30000[u'最近回廠里程數'] = df_30000[u'最近回廠里程數'].map(lambda x: int(x/50000)*50000  ).astype(str)

    # df_30000[u'年式'] = df_30000[u'年式'].map(lambda x: u'1900' if (x<1987 or ) else x)

    df_30000_ND = df_30000.drop_duplicates(u'使用人手機') #476重複 因為僅有後六碼 但 登入的沒有重複者

    #------------------------------------------------------loading finish
    
    #df_click = df_click[[u'手機號碼', 'click']]
    
    df_10000_1 = df_10000.merge(df_click, how='left', left_on=u'使用人手機' , right_on=u'手機號碼')
    df_10000_1['click'] = df_10000_1['click'].fillna(0)
    df_10000_1 = df_10000_1[df_10000_1['click']==1]
    #df_10000_1[df_10000_1[u'兌換狀態']=='Y'].shape[0]
    
    df_our = df_10000_1
    
    df_10316_1 = df_10316.merge(df_click, how='left', left_on=u'MOBILE' , right_on=u'手機號碼')
    df_10316_1['click'] = df_10316_1['click'].fillna(0)
    df_10316_1 = df_10316_1[df_10316_1['click']==1]
    
    df_joint = df_10316_1[df_10316_1['is_alreadysend_in3w']==1]
    
    
    df_click_1 = df_click.merge(df_10316,  how='left', left_on=u'手機號碼' , right_on=u'MOBILE')
    df_click_1['is_in10316'] = df_click_1['is_in10316'].fillna(0)
    df_toyota = df_click_1[df_click_1['is_in10316']==0]
    
    df_toyota[u'手機號碼'] = df_toyota[u'手機號碼'].astype(str).str[-6:] #891
    df_toyota =df_toyota.drop_duplicates(u'手機號碼') #0重複
    df_toyota =  df_toyota.merge(df_30000_ND,  how='left', left_on=u'手機號碼' , right_on=u'使用人手機')
    
    df_our_get = df_our[df_our[u'兌換狀態']=='Y']
    df_toyota_get = df_toyota[df_toyota[u'兌換狀態']=='Y']
    df_joint_get  = df_joint[df_joint[u'兌換狀態']=='Y']
    
    result_dict = {'df_10000':df_10000, 'df_10316':df_10316,'df_30000':df_30000_ND, 'df_toyota':df_toyota, 'df_joint':df_joint, 'df_our':df_our, 'df_toyota_get':df_toyota_get, 
                   'df_joint_get':df_joint_get, 'df_our_get':df_our_get}

    return result_dict

'''

df_our
Index([           u'序號',         u'CR經銷商',         u'CR營業所',          u'CR業代',
               u'使用人ID',         u'使用人姓名',       u'使用人郵遞區號',         u'使用人地址',
               u'使用人電話',         u'使用人手機',            u'碼數',         u'是否為手機',
               u'使用人性別',         u'使用人年齡',          u'車牌號碼',         u'車牌號碼2',
             u'車牌號碼txt',      u'車牌號碼txt2',        u'車牌+*調整',          u'車牌+*',
             u'車牌COUNT',            u'車名',            u'年式',          u'登錄日期',
                u'交車日期',           u'登錄年',       u'最近回廠里程數',        u'最近回廠日期',
               u'最近回廠年',     u'強制險最近保險日期',     u'任意險最近保險日期',         u'最近保險年',
           u'最近回廠or保險年',       u'使用人CR分級',     u'投保車體險或任意險',      u'每年連續回廠定保',
       u'2台(含)以上且同一DLR',          u'服務分級',    u'是否為altis名單',          u'手機重複',
                   u'#',          u'重新發送',            u'性別',          u'手機號碼',
             u'預計到訪營業所',            u'通路',          u'預約時間',          u'重發時間',
                u'兌換時間',         u'兌換營業所',          u'兌換狀態',            u'來源',
                  u'備註',   u'二次發送QR code',         u'click'],
      dtype='object')

df_toyota
Index([                  u'#',                u'重新發送',                u'性別_x',
                      u'手機號碼',             u'預計到訪營業所',                  u'通路',
                      u'預約時間',                u'重發時間',                u'兌換時間',
                     u'兌換營業所',                u'兌換狀態',                  u'來源',
                        u'備註',         u'二次發送QR code',               u'click',
               u'MASK_LICSNO',              u'LICSNO',              u'MOBILE',
       u'is_alreadysend_in3w',                 u'經銷商',                 u'營業所',
                      u'郵遞區號',                u'性別_y',              u'BRTHDT',
                        u'年齡',                u'車名_x',                u'年式_x',
                    u'交車日期_x',           u'最近回廠里程數_x',            u'最近回廠日期_x',
                   u'最近回廠年_x',                u'CR等級',         u'投保車體險或任意險_x',
                      u'服務等級',          u'is_in10316',                  u'序號',
                     u'CR經銷商',               u'CR營業所',               u'使用人手機',
                     u'使用人性別',               u'使用人年齡',                u'車名_y',
                      u'年式_y',                u'登錄日期',              u'交車日期_y',
                       u'登錄年',           u'最近回廠里程數_y',            u'最近回廠日期_y',
                   u'最近回廠年_y',           u'強制險最近保險日期',           u'任意險最近保險日期',
                     u'最近保險年',           u'最近回廠or保險年',             u'使用人CR分級',
               u'投保車體險或任意險_y',            u'每年連續回廠定保',       u'2台(含)以上且同一DLR'],
      dtype='object')

'''

result_dict = df_get_data()

df_10000 =  result_dict['df_10000']
df_our =  result_dict['df_our']
df_our_get =  result_dict['df_our_get']
df_30000_ND =  result_dict['df_30000']
df_toyota =  result_dict['df_toyota']
df_toyota_get =  result_dict['df_toyota_get']

df_joint_get =  result_dict['df_joint_get']
df_joint =  result_dict['df_joint']

df_10316 = result_dict['df_10316']




df_10000_order = pd.read_csv(u'./cutted/maxnerva_order.csv',  sep=",", encoding="utf-8")
df_30000_order = pd.read_csv(u'./cutted/toyota_order_phone.csv',  sep=",", encoding="utf-8")
df_30000_order = df_30000_order.fillna(0)



df_10000 = df_10000.merge(df_30000_order[[u'手機號碼', u'all']], how='left', left_on=u'使用人手機' , right_on=u'手機號碼')
df_10000_order = df_10000[df_10000['all']==1]


df_10316 = df_10316.merge(df_30000_order[[u'手機號碼', u'all']], how='left', left_on=u'MOBILE' , right_on=u'手機號碼')
df_10316_order = df_10316[df_10316['all']==1]

df_30000_order[u'手機號碼'] = df_30000_order[u'手機號碼'].astype(str).str[-6:]
df_30000_ND =df_30000_ND.merge(df_30000_order[[u'手機號碼', u'all']], how='left', left_on=u'使用人手機' , right_on=u'手機號碼')

df_30000_ND = df_30000_ND.rename({u"最近回廠里程數_x":u"最近回廠里程數"})
df_30000_order = df_30000_ND[df_30000_ND['all']==1]






def cate_sign_chart(title_name = u'車名', df_toyota = df_toyota[u'車名_y'], df_our = df_our[u'車名'], rot=90):
    # df_toyota = df_toyota.astype(str).replace(' ', '')
    # df_our = df_our.astype(str).replace(' ', '')

    #plt.figure()
    a=((df_toyota.value_counts(dropna=True)/28747)).rename(u"販行")
    b= (df_our.value_counts(dropna=True)/9114).rename(u"雲智匯")

    df_ab = pd.concat([a, b], axis=1)
    df_ab = df_ab[df_ab.index!=u'nan']

    axes = df_ab.plot.bar( mark_right=False, rot=rot, alpha=0.5, subplots=True, sharex=True, sharey=True)

    axes[0].set_xlabel(title_name,fontproperties=myfont)
    axes[0].set_ylabel(u'登錄人數比例(佔總發送人數)',fontproperties=myfont)
    axes[0].set_title(u'販行'+title_name+u'登錄率',fontproperties=myfont)
    axes[0].legend(prop=myfont)


    df_ab = df_ab.fillna(0)
    list_anno =((df_ab[u'販行'])*100).astype(str).str[:4]
    list_anno += '%'
    list_anno = list_anno.tolist()
    index=0
    for p in axes[0].patches:
        axes[0].annotate(list_anno[index], (p.get_x() * 1.005, p.get_height() * 1.005))
        index+=1

    axes[1].set_xlabel(title_name,fontproperties=myfont)
    axes[1].set_ylabel(u'登錄人數比例(佔總發送人數)',fontproperties=myfont)
    axes[1].set_title(u'雲智匯'+title_name+u'登錄率',fontproperties=myfont)
    axes[1].legend(prop=myfont)

    list_anno =((df_ab[u'雲智匯'])*100).astype(str).str[:4]
    list_anno += '%'
    list_anno = list_anno.tolist()
    index=0
    for p in axes[1].patches:
        axes[1].annotate(list_anno[index], (p.get_x() * 1.005, p.get_height() * 1.005))
        index+=1

    plt.xticks(fontproperties=myfont)

    plt.show()


def given_cate_sign_chart(title_name = u'車名', df_toyota = df_toyota[u'車名_y'], df_our = df_our[u'車名'], df_30000_ND = df_30000_ND[u'車名'], df_10000= df_10000[u'車名'], rot=90):
  df_toyota = df_toyota.str.replace(' ', '')
  df_our= df_our.str.replace(' ', '')
  df_30000_ND = df_30000_ND.str.replace(' ', '')
  df_10000= df_10000.str.replace(' ', '')

  #plt.figure()
  a=((df_toyota.value_counts())).rename(u"販行")
  b= (df_our.value_counts()).rename(u"雲智匯")
  c = ((df_30000_ND.value_counts())).rename(u"販行30000")
  d = ((df_10000.value_counts())).rename(u"雲智匯10000")

  a=(a/c).rename(u"販行")
  b=(b/d).rename(u"雲智匯")

  df_ab = pd.concat([a, b], axis=1)
  df_ab = df_ab.fillna(0)
  df_ab = df_ab[df_ab.index!=u'nan']
  # df_ab.index = df_ab.index.astype(int) #為了將年齡排序使用
  # df_ab.sort_index(inplace=True)

  axes = df_ab.plot.bar( mark_right=False, rot=rot, alpha=0.5, subplots=True, sharex=True, sharey=True)

  axes[0].set_xlabel(title_name,fontproperties=myfont)
  axes[0].set_ylabel(u'登錄人數比例(佔各別發送人數)',fontproperties=myfont)
  axes[0].set_title(u'販行'+title_name+u'登錄率',fontproperties=myfont)
  axes[0].legend(prop=myfont)

  df_ab = df_ab.fillna(0)
  list_anno =((df_ab[u'販行'])*100).astype(str).str[:4]
  list_anno += '%'
  list_anno = list_anno.tolist()
  index=0
  for p in axes[0].patches:
      axes[0].annotate(list_anno[index], (p.get_x() * 1.005, p.get_height() * 1.005))
      index+=1

  axes[1].set_xlabel(title_name,fontproperties=myfont)
  axes[1].set_ylabel(u'登錄人數比例(佔各別發送人數)',fontproperties=myfont)
  axes[1].set_title(u'雲智匯'+title_name+u'登錄率',fontproperties=myfont)
  axes[1].legend(prop=myfont)

  list_anno =((df_ab[u'雲智匯'])*100).astype(str).str[:4]
  list_anno += '%'
  list_anno = list_anno.tolist()
  index=0
  for p in axes[1].patches:
      axes[1].annotate(list_anno[index], (p.get_x() * 1.005, p.get_height() * 1.005))
      index+=1

  plt.xticks(fontproperties=myfont)

  plt.show()

  return None

def given_order_chart(title_name = u'車名', df_10000 = df_10000, df_30000_ND= df_30000_ND, rot=90):
  # df_30000_ND = df_30000_ND.str.replace(' ', '')
  # df_10000= df_10000.str.replace(' ', '')
  # df_10000[title_name] = df_10000[title_name].astype(float).astype(int)
  # df_30000_ND[title_name] = df_30000_ND[title_name].astype(float)



  # df_10000[title_name] = df_10000[title_name].str[:-2] #給年式修正用

  #plt.figure()
  a = ((df_10000[title_name][df_10000['all']==1].value_counts())).rename(u"雲智匯order")
  b = ((df_10000[title_name].value_counts())).rename(u"雲智匯10000")
  c = ((df_30000_ND[title_name][df_30000_ND['all']==1].value_counts())).rename(u"販行order")
  d = ((df_30000_ND[title_name].value_counts())).rename(u"販行30000")


  a=(a/b).rename(u"雲智匯")
  c=(c/d).rename(u"販行")

  e=(a*(1-a))/b
  e=(e**.5).rename(u"雲智匯std")

  f=(c*(1-c))/d
  f=(f**.5).rename(u"販行std")


# sqrt(p(1-p)/n)===================

  df_ab = pd.concat([a,b,c,d,e,f], axis=1)
  df_ab = df_ab.fillna(0)
  df_ab = df_ab[df_ab.index!=u'nan']
  df_ab.index = df_ab.index.astype(float).astype(int) #為了將年齡排序使用
  df_ab.sort_index(inplace=True)



  axes = df_ab[[u"雲智匯", u"販行"]].plot.bar( mark_right=False, rot=rot, alpha=0.5, subplots=True, sharex=True, sharey=True,ylim=(0,0.10) )

  axes[0].set_xlabel(title_name+u' 比例與總發送人數',fontproperties=myfont)
  axes[0].set_ylabel(u'下訂比例 (除以該區發送數)',fontproperties=myfont)
  axes[0].set_title(u'雲智匯'+u'下訂率'+' by '+title_name,fontproperties=myfont)
  axes[0].legend(prop=myfont)

  df_ab = df_ab.fillna(0)
  list_anno =((df_ab[u'雲智匯'])*100).astype(str).str[:4]
  list_anno += '% '
  list_anno += df_ab[u'雲智匯10000'].astype(str)
  list_anno = list_anno.tolist()
  index=0
  for p in axes[0].patches:
      axes[0].annotate(list_anno[index], (p.get_x() * 1.00, p.get_height() * 1.005))
      index+=1
  index=0
  for p in axes[0].patches:
      axes[0].annotate(u'-----------', (p.get_x() * 1.00, p.get_height() + 2.0*df_ab[u'雲智匯std'].iloc[index]))
      index+=1

  index=0
  for p in axes[0].patches:
      axes[0].annotate(u'-----------', (p.get_x() * 1.00,   (p.get_height() - 2.0*df_ab[u'雲智匯std'].iloc[index] if (p.get_height() - 2.0*df_ab[u'雲智匯std'].iloc[index])>0 else 0) ))
      index+=1


  axes[1].set_xlabel(title_name+u' 比例與總發送人數',fontproperties=myfont)
  axes[1].set_ylabel(u'下訂比例 (除以該區發送數)',fontproperties=myfont)
  axes[1].set_title(u'販行'+u'下訂率'+' by '+title_name,fontproperties=myfont)
  axes[1].legend(prop=myfont)


  df_ab = df_ab.fillna(0)
  list_anno =((df_ab[u'販行'])*100).astype(str).str[:4]
  list_anno += '% '
  list_anno += df_ab[u'販行30000'].astype(str)
  list_anno = list_anno.tolist()
  index=0
  for p in axes[1].patches:
      axes[1].annotate(list_anno[index], (p.get_x() * 1.00, p.get_height() * 1.005))
      index+=1
  index=0
  for p in axes[1].patches:
      axes[1].annotate(u'-----------', (p.get_x() * 1.00, p.get_height() + 2.0*df_ab[u'販行std'].iloc[index]))
      index+=1

  index=0
  for p in axes[1].patches:
      axes[1].annotate(u'-----------', (p.get_x() * 1.00, (p.get_height() - 2.0*df_ab[u'販行std'].iloc[index] if (p.get_height() - 2.0*df_ab[u'販行std'].iloc[index])>0 else 0)))
      index+=1

  plt.xticks(fontproperties=myfont)
  # plt.tight_layout()
  plt.show()

  return None  

def two_ft_order_rate_chart(title_name = u'兩特徵訂車率', f1=u"年式", f2=u"最近回廠年", df_send = df_30000_ND, df_order= df_30000_order):

  df_grouped_order = df_order.groupby([f1, f2]).size().rename(u"order")
  df_grouped_send = df_send.groupby([f1, f2]).size().rename(u"send")
  # df_grouped = df_grouped.reset_index()
  df_ab = pd.concat([df_grouped_order,df_grouped_send], axis=1)
  df_ab = df_ab.fillna(0) 
  df_ab[u"order"]=df_ab[u"order"].astype(int)
  df_ab[u"send"]=df_ab[u"send"].astype(int)
  df_ab[u"ratio"]=df_ab[u"order"]/df_ab[u"send"]

  df_ab = df_ab.reset_index()
  df_ab[f1]=df_ab[f1].astype(float)
  df_ab[f2]=df_ab[f2].astype(float)

  axes= df_ab.plot.scatter(x=f1, y=f2, s=df_ab[u'ratio']*20000)
  # axes= df_ab.plot.scatter(x=f1, y=f2, c=u'ratio', s=60 )

  list_anno =((df_ab[u'ratio'])*100).astype(str).str[:4]
  list_anno += '% '

  list_anno = [ s.replace('0.0% ', ' ') if s=='0.0% ' else s for s in list_anno]

  for index in xrange(0, df_ab.shape[0]):
      axes.annotate(list_anno[index]  ,xy=(df_ab[f1].iloc[index],df_ab[f2].iloc[index]))

  axes.set_xlabel(f1,fontproperties=myfont)
  axes.set_ylabel(f2,fontproperties=myfont)
  axes.ticklabel_format(useOffset=False)
  axes.set_title(title_name,fontproperties=myfont)
  # axes.legend(u'123', prop=myfont)
  # plt.gca().invert_xaxis()
  plt.show()

  return None
 
  
#print df_10000[u'最近回廠年'].value_counts()
# cate_sign_chart(title_name = u'車名', df_toyota = df_toyota[u'車名_y'], df_our = df_our[u'車名'], rot=90)
# cate_sign_chart(title_name = u'CR經銷商', df_toyota = df_toyota[u'CR經銷商'], df_our = df_our[u'CR經銷商'], rot=0)
# cate_sign_chart(title_name = u'使用人性別', df_toyota = df_toyota[u'使用人性別'], df_our = df_our[u'使用人性別'], rot=0)
# cate_sign_chart(title_name = u'年式', df_toyota = df_toyota[u'年式_y'].astype(str), df_our = df_our[u'年式'].astype(str), rot=0)
# cate_sign_chart(title_name = u'最近回廠年', df_toyota = df_toyota[u'最近回廠年_y'].astype(str), df_our = df_our[u'最近回廠年'].astype(str), rot=0)
# cate_sign_chart(title_name = u'使用人CR分級', df_toyota = df_toyota[u'使用人CR分級'].astype(str), df_our = df_our[u'使用人CR分級'].astype(str), rot=0)
# cate_sign_chart(title_name = u'投保車體險或任意險', df_toyota = df_toyota[u'投保車體險或任意險_y'].astype(str), df_our = df_our[u'投保車體險或任意險'].astype(str), rot=0)



#以下畫圖方法 因為除以 每個單位的發送數量 因此不受數量多寡的影響
# given_cate_sign_chart(title_name = u'車名', df_toyota = df_toyota[u'車名_y'], df_our = df_our[u'車名'], df_30000_ND = df_30000_ND[u'車名'], df_10000= df_10000[u'車名'], rot=90)
# given_cate_sign_chart(title_name = u'CR經銷商', df_toyota = df_toyota[u'CR經銷商'], df_our = df_our[u'CR經銷商'], df_30000_ND = df_30000_ND[u'CR經銷商'], df_10000= df_10000[u'CR經銷商'], rot=0)
# given_cate_sign_chart(title_name = u'使用人性別', df_toyota = df_toyota[u'使用人性別'], df_our = df_our[u'使用人性別'], df_30000_ND = df_30000_ND[u'使用人性別'], df_10000= df_10000[u'使用人性別'], rot=0)
# given_cate_sign_chart(title_name = u'年式', df_toyota = df_toyota[u'年式_y'], df_our = df_our[u'年式'], df_30000_ND = df_30000_ND[u'年式'], df_10000= df_10000[u'年式'], rot=0)
# given_cate_sign_chart(title_name = u'最近回廠年', df_toyota = df_toyota[u'最近回廠年_y'], df_our = df_our[u'最近回廠年'], df_30000_ND = df_30000_ND[u'最近回廠年'], df_10000= df_10000[u'最近回廠年'], rot=0)
# given_cate_sign_chart(title_name = u'使用人CR分級', df_toyota = df_toyota[u'使用人CR分級'], df_our = df_our[u'使用人CR分級'], df_30000_ND = df_30000_ND[u'使用人CR分級'], df_10000= df_10000[u'使用人CR分級'], rot=0)
# given_cate_sign_chart(title_name = u'投保車體險或任意險', df_toyota = df_toyota[u'投保車體險或任意險_y'], df_our = df_our[u'投保車體險或任意險'], df_30000_ND = df_30000_ND[u'投保車體險或任意險'], df_10000= df_10000[u'投保車體險或任意險'], rot=0)
# given_cate_sign_chart(title_name = u'使用人年齡', df_toyota = df_toyota[u'使用人年齡'], df_our = df_our[u'使用人年齡'], df_30000_ND = df_30000_ND[u'使用人年齡'], df_10000= df_10000[u'使用人年齡'], rot=0)


# given_order_chart(title_name = u'使用人CR分級', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=10) 
# given_order_chart(title_name = u'車名', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=90) 
# given_order_chart(title_name = u'CR經銷商', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=10)  
# # given_order_chart(title_name = u'使用人性別', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=10)
# given_order_chart(title_name = u'年式', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=10)
# given_order_chart(title_name = u'最近回廠年', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=10)
# given_order_chart(title_name = u'投保車體險或任意險', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=10)
given_order_chart(title_name = u'最近回廠里程數', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=90)
# given_order_chart(title_name = u'使用人年齡', df_10000= df_10000, df_30000_ND = df_30000_ND, rot=10)


# def two_feature_order_chart(title_name = u'車名', df_10000 = df_10000, df_30000_ND= df_30000_ND, rot=90):




# two_ft_order_rate_chart(title_name = u'販行兩特徵訂車率', f1=u"年式", f2=u"最近回廠年", df_send = df_30000_ND, df_order= df_30000_order)
# two_ft_order_rate_chart(title_name = u'雲智匯兩特徵訂車率', f1=u"年式", f2=u"最近回廠年",df_send = df_10000, df_order= df_10000_order)

# two_ft_order_rate_chart(title_name = u'販行兩特徵訂車率', f1=u"年式", f2=u"使用人年齡", df_send = df_30000_ND, df_order= df_30000_order)
# two_ft_order_rate_chart(title_name = u'雲智匯兩特徵訂車率', f1=u"年式", f2=u"使用人年齡",df_send = df_10000, df_order= df_10000_order)

# two_ft_order_rate_chart(title_name = u'販行兩特徵訂車率', f1=u"年式", f2=u"最近回廠里程數", df_send = df_30000_ND, df_order= df_30000_order)
# two_ft_order_rate_chart(title_name = u'雲智匯兩特徵訂車率', f1=u"年式", f2=u"最近回廠里程數",df_send = df_10000, df_order= df_10000_order)

# two_ft_order_rate_chart(title_name = u'販行兩特徵訂車率', f1=u"使用人年齡", f2=u"最近回廠里程數", df_send = df_30000_ND, df_order= df_30000_order)
# two_ft_order_rate_chart(title_name = u'雲智匯兩特徵訂車率', f1=u"使用人年齡", f2=u"最近回廠里程數",df_send = df_10000, df_order= df_10000_order)

# two_ft_order_rate_chart(title_name = u'販行兩特徵訂車率', f1=u"使用人年齡", f2=u"最近回廠年", df_send = df_30000_ND, df_order= df_30000_order)
# two_ft_order_rate_chart(title_name = u'雲智匯兩特徵訂車率', f1=u"使用人年齡", f2=u"最近回廠年",df_send = df_10000, df_order= df_10000_order)

# two_ft_order_rate_chart(title_name = u'販行兩特徵訂車率', f1=u"最近回廠里程數", f2=u"最近回廠年", df_send = df_30000_ND, df_order= df_30000_order)
# two_ft_order_rate_chart(title_name = u'雲智匯兩特徵訂車率', f1=u"最近回廠里程數", f2=u"最近回廠年",df_send = df_10000, df_order= df_10000_order)







# df_10000 = pd.read_csv(u'./cutted/1萬筆_經銷商_to雲智匯(正確經銷商)_fix.csv',  sep=",", encoding="utf-8")

# #更新里程數   最近回廠年
# df_10000[u'最近回廠里程數'] = df_10000[u'最近回廠里程數'].fillna(0).astype(float)
# df_10000[u'最近回廠年'] = df_10000[u'最近回廠年'].fillna(0).astype(float)
# df_10000[u'年式'] = df_10000[u'年式'].fillna(0).astype(float)

# df_10000[u'year_diff'] = df_10000[u'最近回廠年'] - df_10000[u'年式']

# df_10000[u'最近回廠里程數'] = df_10000.apply (lambda x: ((2016.0-x[u'最近回廠年'])*(x[u'最近回廠里程數']/x[u'year_diff'])+x[u'最近回廠里程數']) if x[u'year_diff']>0 else x[u'最近回廠里程數'], axis=1).astype(int)
